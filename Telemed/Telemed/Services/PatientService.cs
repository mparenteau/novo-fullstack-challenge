﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utils.Database;
using Utils.Models;

namespace TelemedApi.Services
{
    public class PatientService
    {
        private readonly IMongoCollection<Patient> patientsCollection;

        public PatientService(DatabaseSettings dbSettings)
        {
            var client = new MongoClient(dbSettings.ConnectionString);
            var database = client.GetDatabase(dbSettings.DatabaseName);

            patientsCollection = database.GetCollection<Patient>(dbSettings.PatientsCollectionName);
        }

        public List<Patient> GetAllPatients()
        {
            return patientsCollection.Find(patient => true).ToList();
        }

        public Patient GetPatient(string id)
        {
            return patientsCollection.Find<Patient>(patient => patient.Id == id).FirstOrDefault();
        }

        public Patient CreatePatient(Patient patient)
        {
            patientsCollection.InsertOne(patient);
            return patient;
        }

        //Update a patient, but not vitals -- With a bit more time, I would have looked into making sure that the state of the db object is good so that pushing vitals doesn't overwrite patient
        //info and vice versa, but the current way is a simple way to ensure that this issue doesn't happen.
        public void UpdatePatient(string id, Patient patient)
        {
            var filter = new BsonDocument("_id", new ObjectId(id));
            var update = Builders<Patient>.Update.Set("FirstName", patient.FirstName)
                .Set("LastName", patient.LastName)
                .Set("Sex", patient.Sex)
                .Set("Age", patient.Age)
                .Set("Email", patient.Email);
            var result = patientsCollection.FindOneAndUpdate(filter, update);
        }

        public void UpdatePatientVitals(string id, Patient patient)
        {
            var filter = new BsonDocument("_id", new ObjectId(id));
            var update = Builders<Patient>.Update.Set("VitalSigns", patient.VitalSigns);
            var result = patientsCollection.FindOneAndUpdate(filter, update);
        }

        public void RemovePatient(string id)
        {
            patientsCollection.DeleteOne(patient => patient.Id == id);
        }
    }
}
